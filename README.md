# Desafio Promobit Front-End Jr

### Como rodar

Fazer um `git clone https://raul_rothschild@bitbucket.org/raul_tonello/promobit.git`

no diretorio `/promobit` rodar o comando `npm i` e depois  `npm start` 

Link on-line [https://promobit-topaz.vercel.app/](https://promobit-topaz.vercel.app/) 

### Tecnologias utilizadas

- [Recoil](https://recoiljs.org/) para store
- [Axios](https://axios-http.com/)
- [ReactRouter](https://reactrouterdotcom.fly.dev/docs/en/v6)
- Uma lib de css que eu utilizava em meu antigo estagio, chamada Monalisa 
- [WakaTime](https://wakatime.com/@a39ff5ab-74f7-4901-a557-16cdd312074f/projects/vvcejcfwgv?start=2022-02-03&end=2022-02-09) Extensao do VScode para monitorar as horas gastas com o desenvolvimento

### Considerações Finais

Fiz tudo que consegui, faltou fazer a paginação do componente `Catalog` mas não deu tempo. Meu css não ficou dos melhores porque tive foco exclusivo no React e na componentização. Como eu trabalho com Vue tem mais ou menos um ano foi divertido o desafio, os conceitos sao os mesmos mas as arquitetura totalmente diferentes. No Vue o conceito de RenderProps e HOC eh muito mais simples com os templates. Nao usei `Styled Componets` porque para mim não tinha necessidade e achei esse `Recoil` bem simples e parecido com o Vuex. Tinha estendido o prazo do desafio para +2 dias mas nao achei justo com a glr que provavelmente va entregar hoje tbm, entao entreguei do jeito que ta ai. 


Valeu pela oportunidade e pelo tempo de vocês xd

#BUYBTC