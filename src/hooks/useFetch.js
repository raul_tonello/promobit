import { useState, useCallback } from 'react';
import api from '../api'

const useFetch = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const [data, setData] = useState(null);

  const request = useCallback(async (options) => {
    let response

    try {
      setError(null);
      setIsLoading(true);

      response = await api.request(options);
    } catch (error) {
      setError(error.message);
      throw new Error(error);

    } finally {

      setData(response.data);
      setIsLoading(false);
      return { response };
    }
  }, []);

  return { isLoading, setIsLoading, error, data, request };
};

export default useFetch;
