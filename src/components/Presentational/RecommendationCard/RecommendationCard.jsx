import { Link } from "react-router-dom";


function RecommendationCard({data: { poster_path, title, id, release_date }}) {
  const getBackgroundImage = () =>{
    return `https://image.tmdb.org/t/p/original/${poster_path}` 
  }

  function formatDate () {
    if(!release_date) return null
    const dArr = release_date.split("-");  
    return dArr[2]+ "/" + dArr[1]+ "/" + dArr[0].substring(2); 
  }

  return (
    <div className="Card">
      <Link to={`/movie/${id}`}>
        <div className="header" style={{backgroundImage: `url(${getBackgroundImage()})`}}></div>
      </Link>
      <div className="content">
        <p className='_mb5'>{title}</p>
        <span>{formatDate()}</span>
      </div>
    </div>
    )
}


export default RecommendationCard;