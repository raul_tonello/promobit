import React from 'react';
import './ImageBackground.scss'

function ImageBackground({ width, height, url }) {

  const styles = () => {
    return {
      width: width ? `${width}px` : '100%',
      height: height ? `${height}px`: '100%',
      backgroundImage: url ?  `url(${url})`: 'none'
    }
  }

  return (
    <>
      <div className='ImageBackground' style={styles()}></div>
    </>
  );
}

export default ImageBackground;