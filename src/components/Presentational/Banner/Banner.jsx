import './Banner.scss'

export default function Banner ({ children }) {
  
  return (
    <section className='Banner'>
      {children}
    </section>
  );
}
