import React, {useEffect} from 'react';

import  './List.scss';

function List({ render, list, style, listLimit, numberByPage }) {
  // const getStyles = ()=> {
  //   return {
  //     flexDirection: col ? 'col': 'row',
  //     flexWrap: wrap ? wrap: 'wrap',
  //     alignItems: align ? align: 'stretch',
  //     justify
  //   }
  // }
  
  const allPaginations = () => {
    return listLimit / numberByPage;
  }

  return (
    <>
      {render(list.reverse().slice(0, listLimit))}
    </>
  );
}

export default List;