import React, { useRef, useEffect} from 'react';

import './Video.scss'
const Video = ({ data: { getMovieVideos }}) => {

  const video = useRef();
  useEffect(() => {
    if(!getMovieVideos.length) return
    
    const videoKey = getMovieVideos?.find((video)=> video.type ==='Trailer')

    video.current.src = videoUrl(videoKey?.key)
  });

  const videoUrl = (key) => {
    return `https://www.youtube.com/embed/${key}?autoplay=0&fs=0&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&start=0&end=0&origin=https://youtubeembedcode.com`
  }

  return (
    <>
      {
      !getMovieVideos.length ? null: (
        <>
          <section className='Video'>
            <div className="container">
              <h2>Trailer</h2>
              <iframe ref={video} width="788.54" height="443" type="text/html" src="https://www.youtube.com/embed/DBXH9jJRaDk?autoplay=0&fs=0&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&start=0&end=0&origin=https://youtubeembedcode.com"></iframe>
            </div>
          </section>
        </>
      )
    }
    </>
  );
}

export default Video;
