import React from 'react';

import './Rating.scss';

function Rating() {
  return (
    <>
      <div className='Rating'>
        <div className='content'>
          <svg width="100" height="100">
            <circle r="25" cx="50" cy="50" className="progress"></circle>
          </svg>
          <small>50%</small>
        </div>
      </div>
    </>
  );
}

export default Rating;