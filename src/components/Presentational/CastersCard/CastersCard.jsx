function CastersCard({data: { profile_path, name, character, original_name }}) {
  const getBackgroundImage = () =>{
    return `https://image.tmdb.org/t/p/original/${profile_path}` 
  }

  const renderNames = () => {
    return (
      <div className="names _mb0">
        {original_name === name ? null : <small className='_mb5'>{original_name}</small>}
        <small className='_mb10'>{character}</small>
      </div>
    )
  }

  return (
    <div className="Card">
      <div className="header" style={{backgroundImage: `url(${getBackgroundImage()})`}}></div>
      <div className="content">
        <p className='_mb5'>{name}</p>
        {renderNames()}
        </div>
      </div>
    )
}


export default CastersCard;