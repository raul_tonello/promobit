import './Card.scss'
import { Link } from "react-router-dom";


function Card({backgroundImageUrl, name, to, release, character, originalName}) {


  const getBackgroundImage = () =>{
    return `https://image.tmdb.org/t/p/original/${backgroundImageUrl}` 
  }

  const renderNames = () => {
    return (
      <div className="names _mb0">
        {originalName === name ? null : <small className='_mb5'>{originalName}/</small>}
        <small className='_mb10'>{character}</small>
      </div>
    )
  }

  function formatDate () {
    if(!release) return null
    const dArr = release.split("-");  
    return dArr[2]+ "/" + dArr[1]+ "/" + dArr[0].substring(2); 
  }
  

  return (
    <div className="Card">
      <Link to={`/movie/${to}`}>
        <div className="header" style={{backgroundImage: `url(${getBackgroundImage()})`}}></div>
      </Link>
      <div className="content">
        <p className='_mb5'>{name}</p>
        {originalName || character ? renderNames(): null}
        </div>
        <span>{formatDate()}</span>
      </div>
    )
}


export default Card;