import './Header.scss'
import { Link } from 'react-router-dom'


export default function Header () {

  return (
    <header className="Header">
      <div className="container">
         <Link to={'/'}>
          <h2 className='_mb0'>TMDB</h2>
         </Link>
      </div>
    </header>
  );
}