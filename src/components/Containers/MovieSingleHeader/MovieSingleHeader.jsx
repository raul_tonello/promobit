import React from 'react';
import moment from 'moment';
import Banner from '../../Presentational/Banner/Banner';
import ImageBackground from '../../../components/Presentational/ImageBackground/ImageBackground';
import Rating from '../../Presentational/Rating/Rating';

import './MovieSingleHeader.scss'

function MovieSingleHeader({ data }) {

  const renderGenres = () => {
    return data.getMovieDetails?.genres.map(({id, name}) => <li key={id}>{name} </li>)
  }

  const renderReleaseDate = () => {
    const dates = data.getMovieReleaseDates?.find(({ iso_3166_1: country }) => country === 'BR' )


    if(!dates) return  <li key={getRandomInt()}>•</li>  
  
    return (
      <>
        <li key={getRandomInt()}>{dates.release_dates[0]?.certification} •</li>  
        <li key={getRandomInt()}>{moment(dates.release_dates[0]?.release_date).format("MM-DD-YYYY")} (BR)  •</li>
      </>
    )
  }

  const duration = () => {
    return  <li key='i'>• {minToHours(data.getMovieDetails.runtime)}</li>
  }
  
  const getBackgroundImage = () =>{
    return `https://image.tmdb.org/t/p/original${data.getMovieDetails.backdrop_path}` 
  }

  const renderCrewAndCast = () => {
    //Las reglas de negocio soy yo xD
    const [principalActor, secondaryActor] = data.getMovieCredits.cast.sort((a, b)=>{
      if (a.popularity > b.popularity) {
        return 1;
      }
      if (a.popularity < b.popularity) {
        return -1;
      }
      return 0;
    })

    const screenPlayer = data.getMovieCredits.crew.find((person)=> person.job === 'Screenplay')
    const writer = data.getMovieCredits.crew.find((person)=> person.department === 'Writing')
    const director = data.getMovieCredits.crew.find((person)=> person.job === 'Director')


    return (
      <>
        <div className="col _col-4">
          <div className="_content">
            <h6 className='_mb5'>{principalActor?.name}</h6>
            <p>Actor</p>
          </div>
        </div>
        <div className="col _col-4">
          <div className="_content">
            <h6 className='_mb5'>{secondaryActor?.name}</h6>
            <p>Actor</p>
          </div>
        </div>
        <div className="col _col-4">
          <div className="_content">
            <h6 className='_mb5'>{director?.name}</h6>
            <p>Director</p>
          </div>
        </div>
        <div className="col _col-4">
          <div className="_content">
            <h6 className='_mb5'>{screenPlayer?.name}</h6>
            <p>Screenplayer</p>
          </div>
        </div>
        <div className="col _col-4">
          <div className="_content">
            <h6 className='_mb5'>{writer?.name}</h6>
            <p>Writer</p>
          </div>
        </div>
      </>
    )
  }

  return (
    <>
      <Banner>
        <div className="container">
          <div className="Row _gutters">
            <div className="col _col-5">
              <div className="content">
                <ImageBackground width={383} height={574} url={getBackgroundImage()} />
              </div>
            </div>
            <div className="col _col-7">
              <h3>{data.getMovieDetails?.title}</h3>
              <div className="info">
                <ul>
                  {renderReleaseDate()}
                  {renderGenres()}
                  {duration()}
                </ul>
              </div>

              <section className="rating">
                <Rating />
                <p>Avaliação dos usuários</p>
              </section>

              <h3 className='_mb5'>Sinopse</h3>
              <p>{data.getMovieDetails?.overview}</p>


              <div className="Row">
                {renderCrewAndCast()}
            </div>
            </div>
            <div className="col">
            </div>
          </div>
        </div>
      </Banner>
    </>
  );
}

export default MovieSingleHeader;

function getRandomInt() {
  return Math.floor(Math.random() * (10000 - 1)) + 1;
}

function minToHours(min) {
  let hour = Math.floor(min/60)
  let minutes = min%60
  
  return `${hour}h ${minutes} m`
}