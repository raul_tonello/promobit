import './Catalog.scss'
import { useEffect, useState } from 'react'
import useFetch from '../../../hooks/useFetch'
import { useSetRecoilState, useRecoilValue } from 'recoil'

//Components
import Card from '../../Presentational/Card/Card'
import Loading from '../../Presentational/Loading/Loading'
import { ReactComponent as RequestError } from '../../../assets/nocon.svg';

//api
import { getMoviePopular } from '../../../api'

//Context
import moviesState from '../../../contexts/states/movieState/movieState';
import filteredMovieState from '../../../contexts/states/movieState/filteredMovieState';

export default function Catalog () {
  const setRMovies = useSetRecoilState(moviesState);
  const rMovies = useRecoilValue(filteredMovieState)

  let { error, isLoading, setIsLoading, request } = useFetch();

  useEffect(() => {
    //Fazer meu useFetch receber um callback do arquivo de API
    const getPopularMovies = async () => {
      
      if(!rMovies.length) {
        const response = await getMoviePopular()
        setRMovies(response)

        return
      }

      return
    }

    getPopularMovies()

    setIsLoading(false)
  }, [request]);
  


  const renderMovies = () => {
    return rMovies?.map(({ id, release_date, original_title, backdrop_path }) => {
      return (
        <div key={id} className="col _col-2">
          <Card backgroundImageUrl={backdrop_path} to={id} name={original_title} release={release_date}/>
        </div> 
      )
    })
  }

  if(error) return <RequestError />

  if(isLoading) return <Loading />

  return (
    <section className='Movies'>
      <div className="container">
        <div className="Row">
          {renderMovies()}
          </div>
        </div>
    </section>
  );
}
