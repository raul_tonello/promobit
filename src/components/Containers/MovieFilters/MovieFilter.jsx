import { useEffect } from 'react'
import { useRecoilState } from 'recoil'
import './MovieFilter.scss'


//hooks
import useFetch from '../../../hooks/useFetch'

//components 
import Loading from '../../Presentational/Loading/Loading'
import genresState from '../../../contexts/states/genresState/genresState'
import filterListState from '../../../contexts/states/filterListState/filterListState'


//context


export default function MovieFilter () {
  let { error, isLoading, setIsLoading,  request } = useFetch();
  const [genresList, setGenresList] = useRecoilState(genresState)
  const [filterList, setFilterList] = useRecoilState(filterListState)
  
  useEffect(() => {
    const fetchMoviesGenres = async () => {
      return await request({
        url: '/genre/movie/list',
        params: {
          api_key:'7d00fbb34f64f6c6beccce427c3266de',
        },
        method: 'GET'
      })
    }


    if(!genresList.length) {
      fetchMoviesGenres().then(r => setGenresList(r.response.data.genres))
      return
    } 
    
    setIsLoading(false)
  }, [request]);


  const handleFilter = ({ target }) => {
    const indexAt = filterList.findIndex(({ id }) => id === parseInt(target.id))
    const selectedGenre = genresList[indexAt]

    if(indexAt >= 0) {
      setFilterList(removeItemAtIndex(filterList, indexAt, selectedGenre))

    } else {
      const indexAt = genresList.findIndex(({ id }) => id === parseInt(target.id))
      setFilterList([...filterList, genresList[indexAt]])
    }

    return 
  }

  const renderButtonFilters = () => {
    let elementList = []

    //fazer um componente pra isso,
    elementList = genresList.map(({ id, name })=>  (
      <button className='Button _mr-10' id={id} style={isSelected(id)} key={id} onClick={handleFilter}>{name}</button>
    ))

    return elementList;
  } 

  const isSelected = (id) => {
    const selectedGenre = filterList.find((genre) => genre.id === id)
    
    if(selectedGenre){
      return { backgroundColor: "#D18000" }
    }

    return {}
  }

  if(error) return <h3>Erro ao carregar filtros xD</h3>
  
  if(isLoading) return <Loading />

  return (
    <section className='Filters'>
      <div className="container">
        <div className="Row">
          <div className="col _text-center">
            <span className='_mb10'>Filtre por:</span>
            {renderButtonFilters()}
            </div>
          </div>
        </div>
    </section>
  );
}


function replaceItemAtIndex(arr, index, newValue) {
  return [...arr.slice(0, index), newValue, ...arr.slice(index + 1)];
}

function removeItemAtIndex(arr, index) {
  return [...arr.slice(0, index), ...arr.slice(index + 1)];
}