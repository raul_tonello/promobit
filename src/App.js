import Main from './routes/pages/main/main'

export default function App() {
  return (
    <div className="App">
      <Main />
    </div>
  );
}