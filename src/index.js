import React from 'react';
import { render } from "react-dom";
import './main.scss';

import Router from '../src/routes/Router'

import { RecoilRoot } from 'recoil'
//contexts

const rootElement = document.getElementById("root");

render(
  <RecoilRoot>
    <Router />
  </RecoilRoot>,

  rootElement
);