import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";

import React from 'react';

//pages
import App from "../App";
import MovieSingle from "../routes/pages/movieSingle/MovieSingle";


const Router = () => {
  return (
    <>
      <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="/movie/:movieId" element={<MovieSingle />} />
      </Routes>
      </BrowserRouter>
    </>
  );
}

export default Router;

