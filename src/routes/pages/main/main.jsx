import Header from "../../../components/Presentational/Header/Header"
import Banner from "../../../components/Presentational/Banner/Banner"
import Catalog from "../../../components/Containers/Catalog/Catalog"
import MovieFilters from "../../../components/Containers/MovieFilters/MovieFilter"

import '../../../components/Containers/Catalog/Catalog.scss'

export default function Main () {
  
  return (
    <main>
      <Header/>
        <Banner display={'flex'}>
          <div className="container">
            <div className="Row">
              <div className="col">
                <div className="content">
                  <h1 className='_text-center _mb40'>Milhões de filmes, séries e pessoas para descobrir. Explore já.</h1>
                    <MovieFilters />
                </div>
              </div>
            </div>
          </div>
        </Banner >
        <section className='Catalog'>
        <div className="container">
            <div className="Row">
              <Catalog />
            </div>
          </div>
        </section>
    </main>
  );
}
