import React, { useEffect, useState } from 'react';

//Coomponents
import Header from "../../../components/Presentational/Header/Header"
import MovieSingleHeader from '../../../components/Containers/MovieSingleHeader/MovieSingleHeader';
import Video from '../../../components/Presentational/Video/Video';
import List from '../../../components/Presentational/List/List';
import RecommendationCard from '../../../components/Presentational/RecommendationCard/RecommendationCard';
import CastersCard from '../../../components/Presentational/CastersCard/CastersCard';

import { useParams } from "react-router-dom";

import useFetch from '../../../hooks/useFetch';

import Loading from '../../../components/Presentational/Loading/Loading';

import { fetchDataMovieSingle } from '../../../api'

//styles
import './MovieSingle.scss';

const MovieSingle = () => {
  let { isLoading, setIsLoading, request } = useFetch();
  const [movieDetails, setMovieDetails] = useState({});

  let params = useParams();

  useEffect(()=> {
    (async () =>  {
      const response = await fetchDataMovieSingle(params)

      setMovieDetails(response)
      setIsLoading(false)
    })();
    
  }, [request, params])


  if(isLoading) return <Loading />

  return (
    <>
      <Header/>
      <section className="MovieSingle">
        <MovieSingleHeader data={movieDetails} />

        <div className="container">
          <section className='Casters'>
            <h2 className='_mb20'>Elenco original</h2>
            <div className="Row _scroll _no-wrap _row">
              <List className='CastersList' list={movieDetails.getMovieCredits.cast} 
                listLimit={10}
                numberByPage={10}
                render={list => {
                  return list.map((listItem) => <CastersCard data={listItem} key={listItem.id} />
                  )
                }}
              />
            </div>
          </section>

          <Video data={movieDetails} />

          <section className='Recommendations'>
            <h2 className='_h1'>Recomendacoes</h2>
            <div className="Row">
              <List
                list={movieDetails.getMovieRecommendations} 
                listLimit={6}
                numberByPage={6}
                render={list => list.slice(0, 7).map((listItem) => {
                  return (
                    <div className='col _col-2'>
                      <RecommendationCard key={listItem.id} data={listItem} />
                    </div>
                  )})
                }
              />
            </div>
          </section>
        </div>
      </section >
    </>
  );
}

export default MovieSingle;
