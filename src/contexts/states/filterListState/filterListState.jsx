import { atom } from 'recoil'

const filterState = atom({
  key: 'filterState',
  default: [],
});


export default filterState