import { selector } from 'recoil'
import filterListState from '../filterListState/filterListState'
import moviesState from './movieState';

const filteredMovieState = selector({
  key: 'filteredMovieState',
  get: ({ get }) => {
    const filter = get(filterListState);
    const list = get(moviesState);
    let filteredMovies = []

    if(!filter.length) return list

    list.forEach((movie, index) => {
      for (const filterId of filter)
        if(movie.genre_ids.includes(filterId.id)) return filteredMovies.push(list[index])
    })

    return filteredMovies
  },
});


export default filteredMovieState