import { atom } from 'recoil'

const genresState = atom({
  key: 'genresState',
  default: [],
});


export default genresState