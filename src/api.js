const axios = require('axios').default;


const api = axios.create({
  baseURL: 'https://api.themoviedb.org/3/',
  timeout: 1000,
  params: {
    api_key:'7d00fbb34f64f6c6beccce427c3266de',
    language:'pt-BR'
  }
});


export const getMovieDetails = (params) => {
  return api({
    url: `movie/${params.movieId}`,
    method: 'GET'
  })
}

export const getMovieReleaseDates = (params) => {
  return api({
    url: `movie/${params.movieId}/release_dates`,
    method: 'GET'
  })
}

export const getMovieReviews = (params) => {
  return api({
    url: `movie/${params.movieId}/reviews`,
    method: 'GET'
  })
}


export const getMovieRecommendations = (params) => {
  return api({
    url: `movie/${params.movieId}/recommendations`,
    method: 'GET',

  })
}

export const getMovieVideos = (params) => {
  return api({
    url: `movie/${params.movieId}/videos`,
    method: 'GET',

  })
}

export const getMovieCredits = (params) => {
  return api({
    url: `movie/${params.movieId}/credits`,
    method: 'GET',

  })
}

export const getMoviePopularByPage = (pageNumber) => {
  return api({
    url: `movie/popular`,
    method: 'GET',
    params: {
      page: pageNumber,
    }
  })
}


export const getMoviePopular = async () => {
  let data = []

  for(let i = 1; i <= 5; i++){
    const response = await getMoviePopularByPage(i)
    data = [...data, ...response.data.results]
  }

  return data
}

export const fetchDataMovieSingle = async (params) => {
  let data = {}

  const endpoints = {
    getMovieDetails, 
    getMovieReleaseDates, 
    getMovieReviews, 
    getMovieRecommendations,
    getMovieVideos,
    getMovieCredits
  }

  try {
    const response  = await Promise.all(Object.values(endpoints).map((fetch) => fetch(params)))

    response.forEach((response, index)=> {
        data[Object.entries(endpoints)[index][0]] = response.data.results ? response.data.results: response.data
      })
  }
  catch (e){
    throw new Error(e)
  }

  return data
}



export default api